import React from 'react'
import './CSS/SignUp.css'
import { Link } from 'react-router-dom'

const SignUp = () => {
    return (
        <div className='loginsignup'>
          <div className="loginsignup-container">
            <h1>Sign Up</h1>
            <div className="fields">
              <input type="text" placeholder='Name' />
              <input type="email" placeholder='Email' />
              <input type="password" placeholder='Password' />
            </div>
            <button>Continue</button>
            <p className='login'>Already have an account? <Link to='/login'><span>Login</span></Link></p>
            <div className="loginsignup-agree">
              <input type="checkbox" name='' id='' />
              <p>I agree to the Terms & Conditions of use & privacy policy.</p>
            </div>
          </div>
            
        </div>
      )
    
}

export default SignUp