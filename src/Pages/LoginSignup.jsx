import React from 'react'
import './CSS/LoginSignup.css'
import { Link } from 'react-router-dom'

const LoginSignup = () => {
  return (
    <div className='loginsignup'>
      <div className="loginsignup-container">
        <h1>Log In</h1>
        <div className="fields">
          {/* <input type="text" placeholder='Name' /> */}
          <input type="email" placeholder='Email' />
          <input type="password" placeholder='Password' />
        </div>
        <button>Continue</button>
        <p className='login'>Haven't register yet? <Link to='/signup'><span>Sign Up</span></Link></p>
        {/* <div className="loginsignup-agree">
          <input type="checkbox" name='' id='' />
          <p>I agree to the Terms & Conditions of use & privacy policy.</p>
        </div> */}
      </div>
        
    </div>
  )
}

export default LoginSignup