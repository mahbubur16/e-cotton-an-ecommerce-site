import React from 'react'
import './DescriptionBox.css'

const DescriptionBox = () => {
  return (
    <div className='descriptionBox'>
        <div className="descriptionBox-navigator">
            <div className="descriptionBox-nav-box">Description</div>
            <div className="descriptionBox-nav-box fade">Reviews (27)</div>
        </div>
        <div className="descriptionBox-description">
            <p>An e-commerce website for a clothing store, equipped with a comprehensive set of features to enhance the online shopping experience.
        It's key features include Shopping Cart, User Authentication, Product Listing, Search and Filter, Reviews and Ratings, Payment Integration, and Proceed to CHECKOUT</p>
        </div>
    </div>
  )
}

export default DescriptionBox